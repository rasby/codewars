/***
 @method Sum
 */
const sum = (a, b) => a + b;
/***
 * main
 */
(() => {
  console.log(sum(2, 555));
})();
